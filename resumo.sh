#/bin/bash

echo "No Linux, quando um arquivo ou diretório é criado, algumas permissões são atribuídas a ele automaticamente."
sleep 3

echo "Essas permissões são divididas em três grupos: O usuário que criou o arquivo (usuário dono), o grupo dono do arquivo (que pode conter vários usuários), e as dos demais usuários que não pertencem ao grupo dono."
sleep 3

echo "Essas permissões podem ser de leitura (r, read), escrita (w, write) e execução (x, execute)."
sleep 4

echo "A permissão de leitura (r) permite visualizar o conteúdo de um arquivo ou diretório, já a permissão de escrita (w) serve para alterar o conteúdo de um arquivo e diretório."
sleep 1

echo "fim"
